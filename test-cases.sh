#!/bin/bash

DENOM=','

declare -A TESTS
TESTS["2.5.1${DENOM}2.1.0"]='true'
TESTS["3.0.0${DENOM}3.0.0"]='true'
TESTS["2.5.1${DENOM}3.0.0"]='false'
TESTS["0.0.0${DENOM}0.0.0"]='true'

if [ ! -d ".terraform" ]; then
    echo "Initializing Terraform"
    terraform init > /dev/null
fi

for i in "${!TESTS[@]}"
do
    VALUES=($(echo $i | tr ${DENOM} " "))
    REQUEST="${VALUES[0]}"
    THRESHOLD="${VALUES[1]}"
    EXPECT="${TESTS[$i]}"

    terraform apply -var="request_semver=${REQUEST}" -var="threshhold_semvar=${THRESHOLD}" > /dev/null
    RESULT=$(terraform output is-greater-or-equal)

    if [ $EXPECT != $RESULT ]; then
        echo "  FAIL:    REQUEST[${REQUEST}] to THRESHOLD[${THRESHOLD}] => RESULT[${RESULT}]==EXPECTED[${EXPECT}]"
    else
        echo "  SUCCESS: REQUEST[${REQUEST}] to THRESHOLD[${THRESHOLD}] => RESULT[${RESULT}]==EXPECTED[${EXPECT}]"
    fi
done
